<?php

namespace Drupal\prompt_to_login\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Event subscriber that redirect anonymous users to the login form.
 *
 * If anonymous users visit a node of the "article" content type,
 * then redirect them to the "/user/login". The problem we had to face:
 * if the module "page_cache" (Internal Page Cache) is using, then after first
 * redirecting a page caches. And for all next anonymous users redirection not
 * happens anymore, and they are able to see this page. To prevent this has been
 * implemented the response policy for page cache. The implementation is here:
 * /srs/PageCache/DenyAnnouncementCachingForAnonymous.php.
 *
 * @see https://www.drupal.org/node/2323571
 */
class PromptToLoginRequestSubscriber implements EventSubscriberInterface {

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match.
   */
  public function __construct(AccountInterface $account, RouteMatchInterface $routeMatch) {
    $this->account = $account;
    $this->routeMatch = $routeMatch;
  }

  /**
   * Redirect anonymous user to the login form from an "article" node.
   *
   * If anonymous users visit a node of the "article" content type,
   * then redirect them to the login form.
   *
   * @param Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Request event.
   */
  public function onRequest(RequestEvent $event) {
    if ($this->routeMatch->getRouteName() !== 'entity.node.canonical') {
      return;
    }

    if ($this->account->isAuthenticated()) {
      return;
    }

    $node = $this->routeMatch->getParameter('node');

    if ($node instanceof NodeInterface) {
      if ($node->bundle() === 'article') {
      }
      $options['query']['destination'] = $event->getRequest()->getPathInfo();
      $response = new RedirectResponse('/user/login');
      $response->send();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // To be able to get a route name in the KernelEvents::REQUEST using the
    // "current_route_match" service, the "priority" value must be 32 or less.
    // Not sure that the following is needed:
    // set the priority to 30 to run before Dynamic Page Cache (priority 27)
    // see https://www.drupal.org/project/simple_access_log/issues/2949814
    $events[KernelEvents::REQUEST][] = ['onRequest', 30];
    return $events;
  }

}
