<?php

namespace Drupal\prompt_to_login\PageCache;

use Drupal\Core\PageCache\ResponsePolicyInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;

/**
 * Deny caching of nodes of "article" content type for anonymous.
 *
 * This policy deny caching of nodes of the "article" content type for
 * anonymous users. This is needed because we need redirect anonymous users to
 * login form. But after redirect a page is cached and for next anonymous
 * visitors of this page redirect doesn't happen anymore. Next anonymous users
 * are able to see this page, that is unacceptable.
 */
class DenyAnnouncementCachingForAnonymous implements ResponsePolicyInterface {

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs response policy.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match.
   */
  public function __construct(AccountInterface $account, RouteMatchInterface $routeMatch) {
    $this->account = $account;
    $this->routeMatch = $routeMatch;
  }

  /**
   * {@inheritdoc}
   */
  public function check(Response $response, Request $request) {
    if ($this->routeMatch->getRouteName() !== 'entity.node.canonical') {
      return;
    }

    if ($this->account->isAuthenticated()) {
      return;
    }

    $node = $this->routeMatch->getParameter('node');
    if (!$node instanceof NodeInterface) {
      return;
    }

    if ($node->bundle() === 'article') {
      return static::DENY;
    }
  }

}
